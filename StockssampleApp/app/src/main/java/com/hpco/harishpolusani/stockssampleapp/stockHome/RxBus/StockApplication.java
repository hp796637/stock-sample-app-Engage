package com.hpco.harishpolusani.stockssampleapp.stockHome.RxBus;

import android.app.Application;

/**
 * Created by harishpolusani on 1/28/18.
 */

public class StockApplication extends Application {
    private RxBus bus;
    private static StockApplication stockApplication;

    @Override
    public void onCreate() {
        super.onCreate();
        stockApplication=this;
        bus = new RxBus();
    }
    public RxBus bus() {
        return bus;
    }

    public static synchronized StockApplication getApp(){
        return stockApplication;
    }

}
