package com.hpco.harishpolusani.stockssampleapp.stockHome.presentation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.hpco.harishpolusani.stockssampleapp.R;
import com.hpco.harishpolusani.stockssampleapp.stockHome.adapter.RvStocksListAdapter;
import com.hpco.harishpolusani.stockssampleapp.stockHome.model.StockRootModel;

import java.util.TreeMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by harishpolusani on 1/27/18.
 *
 * Name:StockListFragment
 * purpose:contains a recyclerview with list of stocks
 */

public class StockListFragment extends Fragment implements StockContract.StockHomeView{

private StockContract.StockHomePresenter mStockPresenter;

    @BindView(R.id.rv_stocks)
    RecyclerView mRVStocks;
    @BindView(R.id.search_view)
    SearchView searchView;
    @BindView(R.id.fab)
    FloatingActionButton mFab;
    @BindView(R.id.swipe_Refresh_Layout)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.progress_bar)
    ProgressBar loadingIndicator;
    private Unbinder unbinder;
    private RvStocksListAdapter rvStocksListAdapter;
    //private StockRootModel stockRootModel;
    private TreeMap<String,StockRootModel> stockDataMap;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        super.onCreate(savedInstanceState);
        mStockPresenter =new StockPresenter(this);
//        mStockPresenter.getSingleStockInfo("AMD");
        mStockPresenter.getAllStocks();


    }

    /**
     * this method handles adding listners to view in this fragment.
     */
    private void initViews() {
         searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
             @Override
             public boolean onQueryTextSubmit(String query) {
                 return false;
             }

             @Override
             public boolean onQueryTextChange(String query) {
                 if(rvStocksListAdapter!=null)
                 rvStocksListAdapter.getFilter().filter(query);
                 return false;
             }
         });

        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refreshList();
            }
        });
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
              refreshList();
            }
        });

    }

    /**
     * this method gets called when the recyclerview dragevent or referesh button was selected. Api call to fetch the new data will be sent .
     */
    private void refreshList() {
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
        }
        if(mStockPresenter !=null){
            mStockPresenter.getAllStocks();
        }

    }

    /**
     * displays the loading indicator until the aoi call resolves
     */
    public void showLoadingIndicator(){
    loadingIndicator.setVisibility(View.VISIBLE);
    searchView.setVisibility(View.GONE);
    mSwipeRefreshLayout.setVisibility(View.GONE);
    mFab.setVisibility(View.GONE);
    mRVStocks.setVisibility(View.GONE);

}

    /**
     * dismisses the loading indicator when the api call
     */
    public void HideLoadingIndicator(){
        if(loadingIndicator!=null)
    loadingIndicator.setVisibility(View.GONE);
        if(searchView!=null)
    searchView.setVisibility(View.VISIBLE);
        if(mSwipeRefreshLayout!=null)
    mSwipeRefreshLayout.setVisibility(View.VISIBLE);
        if(mFab!=null)
    mFab.setVisibility(View.VISIBLE);
        if(mRVStocks!=null)
    mRVStocks.setVisibility(View.VISIBLE);
}
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_stock_list, container ,false);
        unbinder=  ButterKnife.bind(this,rootView);
        initViews();
        if(stockDataMap==null)
        showLoadingIndicator();
        return rootView;
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(stockDataMap!=null){
            loadListView(stockDataMap);
        }
    }

    /**
     * This method gets called after the successful result from the service call .
     * @param map Model to load the adapter
     */
    @Override
    public void loadListView(TreeMap<String, StockRootModel> map) {
        HideLoadingIndicator();
        stockDataMap=map;
        rvStocksListAdapter=new RvStocksListAdapter(stockDataMap);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        mRVStocks.setLayoutManager(mLayoutManager);
        mRVStocks.setItemAnimator(new DefaultItemAnimator());
        mRVStocks.setAdapter(rvStocksListAdapter);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    /**
     * shows a toast message when network error occurs .
     */
    @Override
    public void onNetworkError() {
        HideLoadingIndicator();
        Toast.makeText(getActivity(),"Call to Fetch Stocks api failed , Please check the network or try after sometime",Toast.LENGTH_LONG).show();
    }

    /**
     * unsubscribing the api subscriptions in this method .
     */
    @Override
    public void onDestroy() {
        if(mStockPresenter!=null ){
            mStockPresenter.unSubscribe();
        }
        super.onDestroy();
        stockDataMap=null;

    }

    /**
     * unsubscribing the view bindings
     *
     */
    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }
}
