package com.hpco.harishpolusani.stockssampleapp.stockHome.RxBus;

import com.hpco.harishpolusani.stockssampleapp.stockHome.model.TimeSeriesDaily;

/**
 * Created by harishpolusani on 1/28/18.
 */

public class RxEvent {
    public TimeSeriesDaily timeSeriesDaily;
    public String symbol;
    public RxEvent(TimeSeriesDaily timeSeriesDaily,String symbol){
        this.timeSeriesDaily=timeSeriesDaily;
        this.symbol=symbol;
    }
}
