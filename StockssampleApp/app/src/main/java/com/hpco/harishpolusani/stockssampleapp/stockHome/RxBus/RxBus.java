package com.hpco.harishpolusani.stockssampleapp.stockHome.RxBus;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by harishpolusani on 1/28/18.
 */

public class RxBus {
    public RxBus() {
    }

    private PublishSubject<Object> bus = PublishSubject.create();

    public void send(Object o) {
        bus.onNext(o);
    }

    public Observable<Object> toObservable() {
        return bus;
    }

}
