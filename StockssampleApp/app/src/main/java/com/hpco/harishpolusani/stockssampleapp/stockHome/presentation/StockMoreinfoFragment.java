package com.hpco.harishpolusani.stockssampleapp.stockHome.presentation;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hpco.harishpolusani.stockssampleapp.R;
import com.hpco.harishpolusani.stockssampleapp.stockHome.model.TimeSeriesDaily;

import java.sql.Time;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by harishpolusani on 1/27/18.
 *
 * Name:StockMoreinfoFragment
 * Purpose: more details of the individual Stock.
 *
 */


public class StockMoreinfoFragment extends Fragment {
private TimeSeriesDaily timeSeriesDaily;
private String symbol;

@BindView(R.id.stock_name)
TextView stockName;
@BindView(R.id.stock_volume)
TextView stockVolume;
@BindView(R.id.stock_open)
TextView stockOpen;
@BindView(R.id.stock_high)
TextView stockHigh;
@BindView(R.id.close)
TextView stockClose;
@BindView(R.id.stock_low)
TextView stockLow;
private Unbinder unbinder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_stock_moreinfo, container ,false);
        unbinder= ButterKnife.bind(this,rootView);
        initviews(timeSeriesDaily);
        return rootView;
    }

    private void initviews(TimeSeriesDaily timeSeriesDaily) {
        stockName.setText("StockName :"+symbol);
        stockLow.setText("Low :"+timeSeriesDaily.get3Low());
        stockOpen.setText("Open :"+timeSeriesDaily.get1Open());
        stockClose.setText("Close :"+timeSeriesDaily.get4Close());
        stockHigh.setText("High :"+timeSeriesDaily.get2High());
        stockVolume.setText("Volume :"+timeSeriesDaily.get5Volume());
    }

    public void setData(TimeSeriesDaily timeSeriesDaily,String symbol) {
        this.timeSeriesDaily=timeSeriesDaily;
        this.symbol=symbol;
    }


    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }
}
