package com.hpco.harishpolusani.stockssampleapp.stockHome.presentation;

import android.support.annotation.NonNull;
import android.support.v4.util.Pair;
import android.util.Log;

import com.hpco.harishpolusani.stockssampleapp.stockHome.model.StockRootModel;
import com.hpco.harishpolusani.stockssampleapp.stockHome.network.NetworkManager;
import com.hpco.harishpolusani.stockssampleapp.stockHome.network.NetworkUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;


import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by harishpolusani on 1/27/18.
 */


public class StockPresenter implements StockContract.StockHomePresenter {

private StockContract.StockHomeView mStockHomeView;
private NetworkManager mNetworkManager;
private CompositeSubscription mComposesubs;
    private List<String> list=new ArrayList<String>(){{add("UNH");add("MA");add("AMZ");add("ADP");add("BBVA");}};
    public  StockPresenter(StockContract.StockHomeView view){
        mStockHomeView=view;
        mNetworkManager=NetworkManager.getInstance();
        mComposesubs=new CompositeSubscription();

    }
public int add(int a,int b){
    return  a+b ;
}
//    /**
//     * This method makes a api call and returns single result for a single stock.
//     * @param symbol  Stocksymbol
//     */
//    @Override
//    public void getSingleStockInfo(String symbol) {
//    Subscription subscription= mNetworkManager.getStockData(NetworkUtils.getAlphaBaseUrl(), NetworkUtils.getStockQueryMap("AMD"), new Subscriber<StockRootModel>() {
//            @Override
//            public void onCompleted() {
//
//            }
//
//            @Override
//            public void onError(Throwable e) {
//                Log.d("Presenter","error");
//
//            }
//
//            @Override
//            public void onNext(StockRootModel stockRootModel) {
////                mStockHomeView.loadListView(stockRootModel);
//            }
//        });
//    mComposesubs.add(subscription);
//    }

    /**
     * this method makes parllel api calls and bundle to results to a List<Pair<String,StockRootModel>>
     */
    @Override
    public void getAllStocks() {

        final Subscription subscription=Observable.from(list)
                .flatMap(new Func1<String, Observable<Pair<String, StockRootModel>>>() {
                    @Override
                    public Observable<Pair<String, StockRootModel>> call(@NonNull String symbol)   {
                        return Observable.zip(
                                Observable.just(symbol),
                                mNetworkManager.getStocksBundle(NetworkUtils.getAlphaBaseUrl(),NetworkUtils.getStockQueryMap(symbol)),
                                new Func2<String, StockRootModel, Pair<String, StockRootModel>>() {
                                    @Override
                                    public Pair<String, StockRootModel> call(@NonNull String id, StockRootModel productResponse)  {
                                        return new Pair<>(id, productResponse);
                                    }
                                });
                    }
                })
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<List<Pair<String, StockRootModel>>>() {
            @Override
            public void onCompleted() {
                Log.d("getAllStocks-Completed","Success");
            }
            @Override
            public void onError(Throwable e) {
              Log.d("getAllStocks-onError",e.getMessage());
              mStockHomeView.onNetworkError();
            }

            @Override
            public void onNext(List<Pair<String, StockRootModel>> pairs) {
                TreeMap<String,StockRootModel> stockDataMap=new TreeMap<>();
                if(pairs!=null){
                    for(Pair<String,StockRootModel> pair:pairs){
                        stockDataMap.put(pair.first,pair.second);
                    }
                }
                mStockHomeView.loadListView(stockDataMap);
                Log.d("getAllStocks-onNext",""+pairs.size());
            }
        });
        mComposesubs.add(subscription);
    }

    /**
     * this method will be called from view to unsubscribe the subscriptions.
     */
    @Override
    public void unSubscribe() {
        mComposesubs.clear();
    }


}
