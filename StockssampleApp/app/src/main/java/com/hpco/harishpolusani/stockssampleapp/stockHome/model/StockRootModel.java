package com.hpco.harishpolusani.stockssampleapp.stockHome.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class StockRootModel {

    @SerializedName("Meta Data")
    @Expose
    private MetaData metaData;
    @SerializedName("Time Series (Daily)")
    @Expose
    private Map<String,TimeSeriesDaily> timeSeriesDaily;

    public MetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }

    public Map<String,TimeSeriesDaily> getTimeSeriesDaily() {
        return timeSeriesDaily;
    }

    public void setTimeSeriesDaily(Map<String,TimeSeriesDaily> timeSeriesDaily) {
        this.timeSeriesDaily = timeSeriesDaily;
    }

}
