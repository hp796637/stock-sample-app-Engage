package com.hpco.harishpolusani.stockssampleapp.stockHome.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.hpco.harishpolusani.stockssampleapp.R;
import com.hpco.harishpolusani.stockssampleapp.stockHome.model.StockRootModel;
import com.hpco.harishpolusani.stockssampleapp.stockHome.model.TimeSeriesDaily;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;


/**
 * Created by harishpolusani on 1/28/18.
 */

public class RvStocksListAdapter extends RecyclerView.Adapter<RvStockViewHolder> implements Filterable {
    private Map<String,StockRootModel> stockDataMap;
    private List<String> stockSymbols;
    private List<String> filteredList;


    public RvStocksListAdapter(Map<String,StockRootModel> map){
        stockDataMap=map;
        loadData(stockDataMap);

    }

    /**
     * this method gets the keys from the map as list , which will be used later to fetch data from map .
     * @param stockDataMap map of key value pair of stocksymbol and related data to that symbol.
     */
    private void loadData(Map<String, StockRootModel> stockDataMap) {
        if(stockDataMap!=null){
            stockSymbols = new ArrayList<>(stockDataMap.keySet()) ;
            Collections.sort(stockSymbols);
            filteredList= stockSymbols;
        }


    }

    @Override
    public RvStockViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_list_item, parent, false);

        return new RvStockViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RvStockViewHolder holder, int position) {
        final Map.Entry<String,TimeSeriesDaily> entry = stockDataMap.get(stockSymbols.get(position)).getTimeSeriesDaily().entrySet().iterator().next();
        holder.bind(position,entry.getValue(), filteredList.get(position));

    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    /**
     * This method filters base on the symbol of the stock.
     * @return Filter of results as pet the charSequence provided as a parameter.
     */
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
              String query=charSequence.toString();
              if(query.isEmpty()){
                 filteredList=stockSymbols;
              }else{
                  List<String> filterList = new ArrayList<>();
                for(String symbol:stockSymbols){
                 if(symbol.toLowerCase().startsWith(query.toLowerCase())){
                     filterList.add(symbol);
                }
}
filteredList=filterList;
              }

                FilterResults filterResults = new FilterResults();
                filterResults.values =filteredList;

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredList = (List<String>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public void updatedata(StockRootModel list){
        notifyDataSetChanged();
    }
}
