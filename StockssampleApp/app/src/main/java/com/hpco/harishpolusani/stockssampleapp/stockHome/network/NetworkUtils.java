package com.hpco.harishpolusani.stockssampleapp.stockHome.network;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by harishpolusani on 1/28/18.
 */

public class NetworkUtils {


    /**
     *
     * @returns Map with query key value pairs which are required for api request .
     */
    public static Map<String,String> getStockQueryMap(String stockSymbol){
Map<String,String> querymap=new HashMap<>();
        querymap.put("function","TIME_SERIES_DAILY");
        querymap.put("symbol",stockSymbol);
        querymap.put("apikey","GJLF 3CX2MZ6ATP09");
        return querymap;
    }

    /**
     * this method could be handled properly instead of returning a raw String at present. considering only this single api call
     * i made it hardcoded.
     * @return String : BaseUrl+path
     */
    public static String getAlphaBaseUrl() {

        return "https://www.alphavantage.co/query/";
    }

}
