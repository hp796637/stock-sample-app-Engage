package com.hpco.harishpolusani.stockssampleapp.stockHome.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.hpco.harishpolusani.stockssampleapp.R;
import com.hpco.harishpolusani.stockssampleapp.stockHome.RxBus.RxEvent;
import com.hpco.harishpolusani.stockssampleapp.stockHome.RxBus.StockApplication;
import com.hpco.harishpolusani.stockssampleapp.stockHome.model.TimeSeriesDaily;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by harishpolusani on 1/28/18.
 */

public class RvStockViewHolder extends RecyclerView.ViewHolder{
@BindView(R.id.symbol_stock)
TextView mSymbol;
@BindView(R.id.cardview)
    CardView cardView;
    @BindView(R.id.stock_volume)
    TextView mVolume;
    @BindView(R.id.stock_open)
    TextView mOpen;
    @BindView(R.id.stock_high)
    TextView mHigh;
    private TimeSeriesDaily timeSeriesDaily;
    private String symbol;

    @BindView(R.id.stock_low)
    TextView mLow;
    public RvStockViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                (StockApplication.getApp())
                        .bus()
                        .send(new RxEvent(timeSeriesDaily ,symbol));
            }
        });
    }



    public void bind(int position, TimeSeriesDaily timeSeriesDaily,String symbol){
        this.symbol=symbol;
        this.timeSeriesDaily=timeSeriesDaily;
       mSymbol.setText(("Symbol :"+symbol));
        mVolume.setText(("Volume :"+timeSeriesDaily.get5Volume()));
        mOpen.setText(("Open : "+timeSeriesDaily.get1Open()));
        mHigh.setText(("High :"+timeSeriesDaily.get2High()));
        mLow.setText(("Low :"+" "+timeSeriesDaily.get3Low()));
    }
}
