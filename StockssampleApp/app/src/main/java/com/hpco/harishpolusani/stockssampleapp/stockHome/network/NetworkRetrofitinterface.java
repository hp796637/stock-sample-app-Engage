package com.hpco.harishpolusani.stockssampleapp.stockHome.network;

import com.hpco.harishpolusani.stockssampleapp.stockHome.model.StockRootModel;

import java.util.Map;

import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by harishpolusani on 1/17/18.
 */

public interface NetworkRetrofitinterface {

    @GET
    Observable<StockRootModel> getIssData(@Url String url, @QueryMap Map<String, String> queryMap);

}
