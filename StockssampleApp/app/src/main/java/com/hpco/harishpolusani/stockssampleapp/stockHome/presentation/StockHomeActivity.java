package com.hpco.harishpolusani.stockssampleapp.stockHome.presentation;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.hpco.harishpolusani.stockssampleapp.R;
import com.hpco.harishpolusani.stockssampleapp.stockHome.RxBus.RxEvent;
import com.hpco.harishpolusani.stockssampleapp.stockHome.RxBus.StockApplication;
import com.hpco.harishpolusani.stockssampleapp.stockHome.model.TimeSeriesDaily;


import java.util.function.Consumer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import rx.functions.Action1;

public class StockHomeActivity extends AppCompatActivity {

    private Fragment mSockListFragment;
    private StockMoreinfoFragment stockMoreinfoFragment;
    private static final String TAG = "StockHomeActivity";
    private static final String STOCK_LIST_FRAGMENT_TAG="StockListFragment";
    private static final String STOCK_MOREINFO_FRAGMENT_TAG="StockMoreInfoFragment";
    private Unbinder unbinder;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder= ButterKnife.bind(this);
        initUI();
        addStockListFragment();
        RegisterBus();
    }

    /**
     * Registering for the rxbus events
     */
    private void RegisterBus() {

        ((StockApplication) getApplication())
                .bus()
                .toObservable()
                .subscribe(new Action1<Object>() {
                    @Override
                    public void call(Object object) {
                        if (object instanceof RxEvent) {
                            addStockinfoFragment(((RxEvent)object).timeSeriesDaily,((RxEvent)object).symbol);
                        }
                    }
                });
    }

    /**
     * This method replaces the StockListFragment with StockinfoFragment when a stock is selected in the list .
     * @param timeSeriesDaily
     */
    private void addStockinfoFragment(TimeSeriesDaily timeSeriesDaily,String symbol) {
 stockMoreinfoFragment=new StockMoreinfoFragment();
stockMoreinfoFragment.setData(timeSeriesDaily,symbol);
FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction().replace(R.id.containe_home,stockMoreinfoFragment,STOCK_MOREINFO_FRAGMENT_TAG);
    fragmentTransaction.addToBackStack(null);
    fragmentTransaction.commitAllowingStateLoss();
    }

    /**
     * This method adds StockListFragment to the activity .
     */
    private void addStockListFragment() {
         mSockListFragment=getSupportFragmentManager().findFragmentByTag(STOCK_LIST_FRAGMENT_TAG);
        if(mSockListFragment==null){
            mSockListFragment=new StockListFragment();
            FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction().add(R.id.containe_home,mSockListFragment,STOCK_LIST_FRAGMENT_TAG);
//            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }

    }

    /**
     * initate the UI or add listeners to the UI
     */
    private void initUI() {
        setSupportActionBar(mToolbar);
    }


    @Override
    public void onBackPressed() {

//        }
        super.onBackPressed();
        if(stockMoreinfoFragment!=null){
            getSupportFragmentManager().beginTransaction().remove(stockMoreinfoFragment).commit();
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
