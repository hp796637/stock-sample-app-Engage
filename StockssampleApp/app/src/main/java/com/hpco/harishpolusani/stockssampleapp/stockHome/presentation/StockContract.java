package com.hpco.harishpolusani.stockssampleapp.stockHome.presentation;

import com.hpco.harishpolusani.stockssampleapp.stockHome.model.StockRootModel;

import java.util.TreeMap;

/**
 * Created by harishpolusani on 1/27/18.
 */

public class StockContract {

    interface StockHomePresenter{
//        void getSingleStockInfo(String symbol);
        void getAllStocks();
        void unSubscribe();

    }

    interface  StockHomeView{
        void loadListView(TreeMap<String,StockRootModel> map);
        void onNetworkError();
    }
}
